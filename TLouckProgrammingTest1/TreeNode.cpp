#include "stdafx.h"
#include "TreeNode.h"

void TreeNode::Clean()
{
	dirty = false;
	if (leftChild)
		leftChild->Clean();
	if (rightChild)
		rightChild->Clean();
}

//recursive constructor generating (but not initializing) the binary tree, depth first. 
TreeNode::TreeNode(TreeNode * thisParent, int levels)
{	
	depth = levels;
	dirty = false;
	parent = thisParent;
	if (levels > 1) {
		leftChild = new TreeNode(this, levels - 1);
		leftChild->isRightChild = false;
		rightChild = new TreeNode(this, levels - 1);
		rightChild->isRightChild = true;
	}
}

TreeNode::~TreeNode()
{
	if (leftChild != NULL)
		delete leftChild;
	if (rightChild != NULL)
		delete rightChild;
}
