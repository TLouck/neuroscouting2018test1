// TLouckProgrammingTest1.cpp : Defines the entry point for the console application.
/*All C++ code in this project written by Tiger Louck
* I wrote this in C++ because it deals with data structures, and C++ is more explicit about copying versus referencing 
* (it's not terrible in C#, but if given the choice...)
*/

#include "stdafx.h"
#include "MathTree.h"

int main()
{
	cout << "General Programming Test: Tree Exercise, Tiger Louck.\n Beginning Operations...\n";
	int depth = 0;
	cout << "Depth of tree: ";
	cin >> depth;
	MathTree tree = MathTree(depth);
	tree.calculateData();
	tree.Print();
	cout << "\nAny key + enter to continue...";
	cin >> depth;
    return 0;
}

