#pragma once
class TreeNode
{
	int remainingLevels;
		
public:
	int data = 0;
	int depth;
	bool dirty;//using this to automate searching, clean objects have not been entered into the stack
	bool isRightChild;//using this to make neighbor searching cheaper at the cost of an extra piece of data per node.
	TreeNode * parent;
	TreeNode* leftChild = NULL;
	TreeNode* rightChild = NULL;
	void Clean();
		
	TreeNode(TreeNode* thisParent, int levels);
	~TreeNode();
};

