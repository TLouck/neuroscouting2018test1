#include "stdafx.h"
#include "MathTree.h"
#include <stack>
#include <vector>

//once the tree is constructed, it's initialized with this
void MathTree::calculateData()
{
	
	root->data = 1;//init the root, to propagate into the tree
	int depth = 0;
	//get a list of lists containing the nodes at each level of the tree
	vector<vector<TreeNode*>> strata = vector<vector<TreeNode*>>(); //C# list equivalent datatype

	//initialize
	strata.push_back(vector<TreeNode*>());
	strata[0].push_back(root);

	//populate
	while (strata[strata.size() - 1][strata[strata.size() - 1].size() - 1]->leftChild != nullptr)//slightly complicated indexer, getting the last entry of the last list, when this fails, we've hit bottom
	{
		strata.push_back(vector<TreeNode*>());
		for (size_t i = 0; i < strata[strata.size() - 2].size(); i++)//for every node on the level above, push their children to a new list from left to right
		{
			strata[strata.size() - 1].push_back(strata[strata.size() - 2][i]->leftChild);
			strata[strata.size() - 1].push_back(strata[strata.size() - 2][i]->rightChild);
		}
	}

	//for each list of nodes, starting from the second, depending on if the node is a right or left child, add the value of node at index/2 and index/2 +- 1 
	for (size_t i = 1; i < strata.size(); i++)
	{
		for (size_t j = 0; j < strata[i].size(); j++)
		{
			//add the guaranteed parent
			strata[i][j]->data += strata[i - 1][j / 2]->data;

			//add either the left or the right parent neighbor
			if (!strata[i][j]->isRightChild) //in this case, left
			{
				//unusual behaviour. where j=0, unless this quantity is encapsulated into the variable you see here 
				//the below conditional will erroneously resolve true, resulting in out-of-bounds accessor use
				int debug = (j / 2) - 1;//never seen that before
				if (debug >= 0)//bounds check
				{
					strata[i][j]->data += strata[i - 1][j / 2 - 1]->data;//get the left parent neighbor and add
				}
			}
			else 
			{
				if (!((j / 2) + 1 >= strata[i-1].size()))//bounds check
				{
					strata[i][j]->data += strata[i - 1][(j / 2) + 1]->data;//get the right parent neighbor and add
				}
			}
		}
	}
}

//depth first printing algorithm, cycles through the entire tree
void MathTree::Print()
{
	//set up a breadth-first queue to order the printing
	stack<TreeNode*> dfStack = stack<TreeNode*>();
	root->dirty = true;
	dfStack.push(root);
	while (dfStack.size() > 0) {
		for (size_t i = 0; i < height - dfStack.top()->depth; i++)//tree formatting
		{
			cout << "|";
		}
		cout << dfStack.top()->data << "\n";
		TreeNode* currentTop = dfStack.top();
		dfStack.pop();

		//stack the children for printing
		if (currentTop->leftChild != nullptr && !currentTop->leftChild->dirty) { //if the current top is null the logic will short and not check the second, avoiding a nullptr exception
			currentTop->leftChild->dirty = true;
			dfStack.push(currentTop->leftChild);
		}
		if (currentTop->rightChild != nullptr && !currentTop->rightChild->dirty) {
			currentTop->rightChild->dirty = true;
			dfStack.push(currentTop->rightChild);
		}
		
		
	}
	root->Clean();
}

MathTree::MathTree(int depth)
{
	root = new TreeNode(nullptr, depth);
	height = depth;
}


MathTree::~MathTree()
{
	delete root;
}
