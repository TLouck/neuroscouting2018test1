#pragma once
#include "TreeNode.h"
#include <stdio.h>

using namespace std;

class MathTree
{
public:
	int height;
	TreeNode* root = NULL;
	void calculateData();
	void Print();
	MathTree(int depth);
	~MathTree();
};

