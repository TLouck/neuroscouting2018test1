# NeuroScouting2018CPP
General Programming exercise for NeuroScouting, done in C++  
  
To Run the Executable: Compiled executable is located in the debug folder of the probject. run the executable and follow the instructions on screen 
  
To view and compile the code: This is a C++ project written in visual studio 2017, so if you have that and doubleclick the .sln file in the root  
you might be able to just open it and compile it without issue. If there's 900+ intelllisense errors in the error window when you open it  
you might need to retarget the solution, so just right click the solution in the solution explorer->retarget. The defaults should be fine.  
If your visual studio install doesn't have C++, open up the Visual Studio Installer and modify your installation to add "Desktop development with C++."  
If some other problem comes up, don't hesitate to ask me about it. (tigerlouck@gmail.com)  

Features of note:  
- The Constructor for the tree nodes is recursive: to instantiate the tree, the tree simply instantiates the root node.  
- The mathematics of the tree's data is accomplished by indexing the entire tree into a series of lists corresponding to its levels, and then indexing into the correct parents that way  
